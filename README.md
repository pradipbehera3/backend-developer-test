# Backend Developer Test
The task is to create **Rest API** to support an e-commerce page that allows the user to search products and place an order.

The system should have fulltext search capability to allow the user to search and each product should have the following properties. 

- Name
- Description 
- Rating 
- Category
- Price

Once the application receives the list of products, the user should be able to place an order, the api should recieve the following peroperties : 

- Order ID (Generated backend side)
- List of Products
- Total Price
- Status (Open, Completed)

The system should also verify if the product exists or/and there are enough items on the stock.

-------

Feel free to spend as much or as little time on the exercise as you like as long as the following requirements have been met.Please complete the user story below and your code should compile and run in one step and unit test must be covered.

Feel free to use whatever frameworks/libraries/packages you like.You must include tests.


User Story

- As a user running the application, <br/>
I can view the products list along with the price information, <br/>
So that I can place an order <br/>

- As a user running the application, <br/>
I can place an order by adding products to the cart, <br/>
So that I can complete the order.

Acceptance criteria 
- [ ] the product must exist 
- [ ] the system should manage the products stock availability 
- [ ] User must be able to create an order
- [ ] User must be able to search products filtered by product name and category
